console.log("Hello World");

// Global Objects
// Arrays  - are global objects

let students = [
	"Tony",
	"Peter",
	"Wanda",
	"Vision",
	"Loki"
]

console.log(students);

// What array method can we use to add an item at the end i the array
students.push("Thor");
console.log(students);

// What array method can we use to add an item at the start of the array?
students.unshift("Steve");
console.log(students);

// What array method does the opposite of push()?
students.pop();
console.log(students);

// What array method does the opposite of unshift()?
students.shift("Steve");
console.log(students);

// What is the difference between splice() and slice()?
	// splice() - removes and adds items from the starting index (Mutator methods)
	// slice() - copies a portion from starting index and return new array from it (Non-mutator method)

// Another kind of array methods?
	// Iterator methods - loops over the item of an array

// forEach() - loops over items in an array and repeats a user-defined function.
// map() - loops over items in an array and repeats a user-defined function AND returns a new array.
// every() - loops and checks of all items satisfy a given condition, returns a boolean value

let arrNum = [15, 20, 25, 30, 11]

// Check if every item in arrNum is divisible by 5
let allDivisible;

arrNum.forEach(num => {
	if(num % 5 === 0) {
		console.log(`${num} is divisible by 5`)
	}
	else {
		allDivisible = false;
	}
	// However, can forEach() return data that will tell is IF all number/items in our arrNum array is divisble by 5?
})
console.log(allDivisible);

arrNum.pop();
arrNum.push(35);
console.log(arrNum);

let divisibleBy5 = arrNum.every(num => {
	console.log(num);
	return num %5 === 0;
})
console.log(divisibleBy5);
// result: true

// Math
// Mathematical constants
// 8 Pre-defined properties which can be called via syntax Math.property (note that properties are case sensitive)
console.log(Math.E); // Euler's Number
console.log(Math.PI); // PI
console.log(Math.SQRT2); // Square root of 2
console.log(Math.SQRT1_2); // Square root of 1/2
console.log(Math.LN2); // Natural logarithm of 2
console.log(Math.LN10); // Natural logarithm of 10
console.log(Math.LOG2E); // Base 2 logarithm of E
console.log(Math.LOG10E); // Base 10 logarithm of E

// Methods for round a number to an integer
console.log(Math.round(Math.PI)); // rounds to a nearest integer; result: 3
console.log(Math.ceil(Math.PI)); // rounds UP to nearest integer; result: 4
console.log(Math.floor(Math.PI)); // rounds DOWN to nearest integer; result: 3
console.log(Math.trunc(Math.PI)); // returns only the integer part (ES6 update); result: 3

// Method for returning the square root of a number
console.log(Math.sqrt(3.14)); // result: 1.77

// Lowest value in a list of arguments
console.log(Math.min(-4, -3, -2, -1, 0, 1, 2, 3, 4)); // result: -4

// Highest value in a list of arguments
console.log(Math.max(-4, -3, -2, -1, 0, 1, 2, 3, 4)); // result: 4



// A C T I V I T Y //

// Function coding 1
// Create a function named addToEnd that will add a passed in string to the end of a passed in array. If element to be added is not a string, return the string "error - can only add strings to an array". Otherwise, return the updated array. Use the students array and the string "Ryan" as arguments when testing.

function addToEnd(array, value){
		if(typeof value !== 'string'){
			return "error - can only add strings to an array";
		}
		else {
			array.push(value);
			return array;
		}
}


// Function coding 2
// Create a function named addToStart that will add a passed in string to the start of a passed in array. If element to be added is not a string, return the string "error - can only add strings to an array". Otherwise, return the updated array. Use the students array and the string "Tess" as arguments when testing.

function addToStart(array, value){
		if(typeof value !== 'string'){
			return "error - can only add strings to an array";
		}
		else {
			array.unshift(value);
			return array;
		}
}


// Function coding 3
// 3. Create a function named elementChecker that will check a passed in array if at least one of its elements has the same value as a passed in argument. If array is empty, return the message "error - passed in array is empty". Otherwise, return a boolean value depending on the result of the check. Use the students array and the string "Jane" as arguments when testing.

function elementChecker(array, value){
    let doesNameExist = students.includes(value);
    if(value === null || value === 0){
        console.log("error - passed in array is empty");
    } 
    else {
        return doesNameExist;
    }
}


// Function coding 4
// Create a function named checkAllStringsEnding that will accept the passed  array and a character. The function will do the ff:

/*
	if array is empty, return "error - array must NOT be empty"
	if at least one array element is NOT a string, return "error - all array elements must be strings"
	if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"
	if 2nd argument is more than 1 character long, return "error - 2nd argument must be a single character"
	if every element in the array ends in the passed character, return true. Otherwise return false.

Use the students array and the character "e" as arguments when testing.

*/

function checkAllStringsEnding(array, value){
	if(array.length === 0){
		return "error - array must NOT be empty";
	}
	else if (!array.every(i => (typeof i === "string"))) {
		return "error - all array elements must be strings";
	}
	else if(typeof value !== "string"){
		return "error - 2nd argument must be of data type string";
	}
	else if(value.length > 1){
		return "error - 2nd argument must be a single character";
	}
	else if(array.every(e => e.charAt(e.length-1) === value)){
		return true;
	}
	else {
		return false;
	}
}

// Function coding 5
// Create a function named stringLengthSorter that will take in an array of strings as its argument and sort its elements in an ascending order based on their lengths. If at least one element is not a string, return "error - all array elements must be strings". Otherwise, return the sorted array. Use the students array to test.

const stringLengthSorter = (arr) => {
    if(arr.some(element => typeof element !== "string")) return "error - all array elements must be strings";
    arr.sort((elementA, elementB) => {
        return elementA.length - elementB.length;
    })
    return arr;
}



